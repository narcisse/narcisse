var describe= func(description,suiteToRun){
        print(description,"\n");
        suiteToRun();
};

var it= func(description,testToRun){
    var msg = "";
    if (!testToRun()[0]) print("*** Err "~description," ",testToRun()[1],"\n");
    else print("   "~description,": Ok\n");
};

var propSet = func(path,msg,compare) {
    var propvalue = getprop(path);
    var msg = "property "~value~msg;
    return [(propvalue == compare),msg];    
};
var expect = func(value) {
   var m = {
           toBe : func(expected) {
               var msg = "got "~value~" expected "~expected;
               return [(value == expected),msg];
           },
           toBeDefined : func() {
               var msg = "should be defined";
               return [(value != nil),msg];
           },
           toBeUnDefined : func() {
               var msg = "should not be defined";
               return [(value == nil),msg];
           },
           propToBe : func(expected) {
               var propvalue = getprop(value);
               var msg = "property "~value~" expected "~expected~" but is "~propvalue;
               return [(propvalue == expected),msg];
           },
           propIsSet : func() {
               return propSet(value," is not set",1);
           },
           propIsNotSet : func() {
               return propSet(value," is set",0);
           }
   };
};