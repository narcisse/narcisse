How to run tests :
------------------

You need to have a standalone nasal interpreter available 
see http://wiki.flightgear.org/Nasal_Unit_Testing_Framework

Download the narcisse project form gitorious : https://gitorious.org/narcisse

Run the test using nasal :

nasal <narcisselocation>/run_test.nas test_nasmine.nas  --narcissedir="<narcisselocation>"

All tests should pass :

****arg : [--narcissedir=/home/jylebleu/devmisc/fg/narcisse/]49145
first try to get nasmine running
   should be able to test equality, this one is ok: Ok
*** Err should be able to test equality, this one is ok got 1 expected 2
   should be able to say hello: Ok
   should be able to check properties: Ok
*** Err should be able to detect invalid properties property controls/engines/engine[0]/reverser-cmd expected 1 but is 0
   should be able to detect a property is set: Ok
   should be able to detect a property is not set: Ok

