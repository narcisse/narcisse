if (caller(0)[0] != globals)
    return call(caller(0)[1], arg, nil, globals);

var tree = {};
var isalpha = func(n) n >= `a` and n <= `z` or n >= `A` and n <= `Z`;
var isdigit = func(n) n >= `0` and n <= `9`;

var sanitize = func(p) {
    if (!p) die();
    if (p[0] == `/`) p = substr(p, 1, nil);
    parts = split("/", p);
    for (var i=0; i<size(parts); i+=1) {
        if (parts[i] == "") {
            if (i == size(parts)-1)
                parts = parts[:i-1];
            else parts = parts[:i-1] ~ parts[i+1:];
            i-=1;
        } else {
            for (var j=0; j<size(parts[i]); j+=1) {
                if (parts[i][j] == `[`) break;
                if (parts[i][j] != `-` and !isalpha(parts[i][j]) and
                        parts[i][j] != `_` and !isdigit(parts[i][j]) and
                        parts[i][j] != `.`) die("bad character in name ");
                }
                if (j == size(parts[i]))
                    parts[i] ~= "[0]";
                elsif (parts[i][-1] != `]`) die("bad index specifier in string ");
        }
    }
    var p = "/";
    foreach (var part; parts)
        p ~= part;
    return p;
}
# wrappers for the FG setprop/getprop APIs:
setprop = func(p, value) tree[ sanitize(p) ] = value;
getprop = func(p) return tree[ sanitize(p) ];
    
var path = ["/foo/bar", "/foo[0]/bar[0]","/foo[0]/bar[0]/", "/foo/bar[0]"];
var value = "MyUniqueValue";
setprop(path[0], value);
foreach(var p; path)
    if (getprop(p) != value) die("sanitize() implementation is broken");

var setlistener = func(prop,initfunc) {
    
}