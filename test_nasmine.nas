load_nasal("mockproputil.nas");
load_nasal("mockprops.nas","props");
load_nasal("nasmine.nas","nsm");

nsm.describe("first try to get nasmine running",func() {
    var a = 1;
    var t = func() return "hello";
    nsm.it("should be able to test equality, this one is ok",func(){
        nsm.expect(a).toBe(1);
    });
    nsm.it("should be able to test equality, this one is ok",func(){
        nsm.expect(a).toBe(2);
    });
    nsm.it("should be able to say hello",func(){
        nsm.expect(t()).toBe("hello");
    });
    nsm.it("should be able to check properties",func(){
        setprop("engines/engine[0]/oil-temperature-degc",52);
        nsm.expect("engines/engine[0]/oil-temperature-degc").propToBe(52);
    });
    nsm.it("should be able to detect invalid properties",func(){
        setprop("controls/engines/engine[0]/reverser-cmd",0);
        nsm.expect("controls/engines/engine[0]/reverser-cmd").propToBe(1);
    });
    nsm.it("should be able to detect a property is set",func(){
        setprop("controls/engines/engine[0]/cutoff",1);
        nsm.expect("controls/engines/engine[0]/cutoff").propIsSet();
    });
    nsm.it("should be able to detect a property is not set",func(){
        setprop("controls/engines/engine[0]/starter",0);
        nsm.expect("controls/engines/engine[0]/starter").propIsNotSet();
    });
});



